/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import {
  StatusBar
} from 'react-native';
import { Provider } from 'react-redux';
import RootNavigator from './src/navigation/RootNavigator';
import configureStore from './src/redux/store/configureStore';

const store = configureStore();

export default class App extends Component {

  render() {
    return (
      <NavigationContainer>
        <Provider store={store}>

          <RootNavigator />

          <StatusBar barStyle="dark-content" />
        </Provider>
      </NavigationContainer>
    );
  }
}