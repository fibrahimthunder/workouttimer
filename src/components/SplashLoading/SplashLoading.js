import LottieView from "lottie-react-native";
import React, { Component } from "react";
import { Image } from "react-native";
import { StatusBar } from "react-native";
import { Platform } from "react-native";
import { View } from "react-native";
import { connect } from "react-redux";
import { ScreenNames } from "../../constants/ScreenNames";
import { splashScreen } from "../../helpers/Images";
import Style from "./splashLoadingStyles";

class SplashLoading extends Component {
  componentDidMount() {
    if (Platform.OS === "android") {
      StatusBar.setTranslucent(true);
      StatusBar.setBackgroundColor("transparent");
    }

    setTimeout(() => {
      // this.props.navigation.replace(ScreenNames.HomeScreen);
      this.props.navigation.replace(ScreenNames.BottomNavigator);
    }, 300 * 1);
  }

  componentDidUpdate(prevProps) {}

  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <LottieView
    style={{ width: 200, height: 200, alignSelf: "center" }}
    ref={animation => {
        this.animation2 = animation;
    }}
    source={require('../../animations/loaderanimation.json')}
    loop={true}
    autoPlay={true}
/> */}

        <Image
          style={[
            {
              width: "100%",
              height: "100%",
              // paddingBottom: 10,
              // paddingTop: 10,
              // paddingRight: 10,
              // marginRight: 10,
            },
          ]}
          source={splashScreen}
        />
      </View>
    );
  }
}

const mapStateToProps = null;

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(SplashLoading);
