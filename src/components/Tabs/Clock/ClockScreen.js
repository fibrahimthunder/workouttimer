import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, TouchableOpacity } from "react-native";
import Style from "./clockScreenStyles";
import {
  AsyncClearAll,
  AsyncGetMultiple,
  AsyncGetViaKey,
  AsyncRemoveViaKey,
  AsyncStoreMultiple,
  AsyncStoreViaKey,
} from "../../../helpers/LocalStorage/AsyncStorage";
import colors from "../../../helpers/colors";
import moment from "moment";
import { Platform } from "react-native";
import { StatusBar } from "react-native";

class ClockScreen extends Component {
  state = {
    curTime: "",
    intervalModeList: [
      {
        name: "Pushups",
        deletedAble: false,
        rounds: 2,
        workTime: {
          hr: 0,
          min: 1,
          sec: 0,
        },
        restTime: {
          hr: 0,
          min: 0,
          sec: 20,
        },
      },
      {
        name: "Dumbles",
        deletedAble: false,
        rounds: 3,
        workTime: {
          hr: 0,
          min: 1,
          sec: 30,
        },
        restTime: {
          hr: 0,
          min: 0,
          sec: 40,
        },
      },
      {
        name: "Planks",
        deletedAble: false,
        rounds: 3,
        workTime: {
          hr: 0,
          min: 0,
          sec: 30,
        },
        restTime: {
          hr: 0,
          min: 1,
          sec: 0,
        },
      },
    ],
  };

  componentDidMount() {
    this.LocalAsync();
    setInterval(() => {
      if (Platform.OS === "android") {
        StatusBar.setTranslucent(false);
        // StatusBar.setBackgroundColor("transparent");
      }

      this.setState({
        curTime: moment(new Date().toLocaleString()).format("hh:mm:ss A"),
      });
    }, 1000);
  }

  LocalAsync() {
    AsyncGetViaKey("key1").then((obj) => {
      if (!!obj) {
        if (obj.length < 1) {
          this.storeDataInDb();
        }
      } else {
        this.storeDataInDb();
      }
    });
  }
  storeDataInDb() {
    AsyncStoreViaKey("key1", this.state.intervalModeList).then((obj) => {
      if (!!obj) {
        // alert("Success")
      } else {
        // alert("Error")
      }
    });
  }
  render() {
    return (
      <View style={Style.container}>
        <View>
          <Text
            style={{
              textAlign: "center",
              color: colors.appTextColor,
              fontFamily:
                Platform.OS === "android"
                  ? "digital-7 (italic)"
                  : "Digital-7Italic",
              fontSize: 60,
            }}
          >
            {this.state.curTime}
          </Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            backgroundColor: colors.appBackground,
            alignSelf: "center",
          }}
        ></View>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { appName } = state.authReducer;
  return {
    appName,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ClockScreen);
