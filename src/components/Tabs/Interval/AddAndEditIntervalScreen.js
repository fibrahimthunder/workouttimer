import Picker from '@gregfrench/react-native-wheel-picker';
import Moment from 'moment';
import React, { Component } from "react";
import {
    Dimensions, Keyboard, SafeAreaView, ScrollView, Text, View
} from "react-native";
import { connect } from "react-redux";
import { strings } from "../../../constants/Localization";
import ButtonFill from '../../../CustomComponents/ButtomFill.js';
import HeaderCenter from "../../../CustomComponents/Header/HeaderCenter.js";
import TextFieldWithRoundedBorder from '../../../CustomComponents/TextFieldWithRoundedBorder.js';
import colors from "../../../helpers/colors";
import {
    backImage
} from '../../../helpers/Images';

var PickerItem = Picker.Item;


class AddAndEditIntervalScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            workoutName: '',
            workoutTime: '',
            restTime: '',
            totalRounds: '',
            isWorkoutTimePickerVisible: false,
            duration: '',
            value: '10:00',

            selectedHoursWorkTime: 0,
            selectedMinutesWorkTime: 0,
            selectedSecWorkTime: 0,

            selectedHoursRestTime: 0,
            selectedMinutesRestTime: 0,
            selectedSecRestTime: 0,

            wheelSelectorItems: []
        };
        this.workoutNameInput = null;
        this.workoutTimeInput = null;
        this.restTimeInput = null;
        this.totalRoundsInput = null;
    }

    componentDidMount() {

        if (!!this.props.route.params.item) {
            this.setState({
                workoutName: this.props.route.params.item.name,
                totalRounds: this.props.route.params.item.rounds.toString(),

                selectedHoursWorkTime: this.props.route.params.item.workTime.hr,
                selectedMinutesWorkTime: this.props.route.params.item.workTime.min,
                selectedSecWorkTime: this.props.route.params.item.workTime.sec,

                selectedHoursRestTime: this.props.route.params.item.restTime.hr,
                selectedMinutesRestTime: this.props.route.params.item.restTime.min,
                selectedSecRestTime: this.props.route.params.item.restTime.sec,
            })
        }

        var numbersArray = []

        for (let i = 0; i < 60; i++) {
            numbersArray.push(i.toString());
        }

        this.setState({
            wheelSelectorItems: numbersArray
        })
    }

    onHoursPickerSelect(index) {
        this.setState({
            selectedHoursWorkTime: index,
        });
    }

    onMinutesPickerSelect(index) {
        this.setState({
            selectedMinutesWorkTime: index,
        });
    }

    onSecondsPickerSelect(index) {
        this.setState({
            selectedSecWorkTime: index,
        });
    }

    onRestHoursPickerSelect(index) {
        this.setState({
            selectedHoursRestTime: index,
        });
    }

    onRestMinutesPickerSelect(index) {
        this.setState({
            selectedMinutesRestTime: index,
        });
    }

    onRestSecondsPickerSelect(index) {
        this.setState({
            selectedSecRestTime: index,
        });
    }

    //REMARK: - Actions
    btnActionSaveInterval = () => {

        const { workoutName, totalRounds, selectedHoursWorkTime, selectedMinutesWorkTime, selectedSecWorkTime, selectedHoursRestTime, selectedMinutesRestTime, selectedSecRestTime } = this.state;

        if (workoutName === "") {
            alert("Workout Name is compulsory!")
        } else if (selectedHoursWorkTime === 0 && selectedMinutesWorkTime === 0 && selectedSecWorkTime === 0) {
            alert("Workout Time can't be zero!")
        } else if (selectedHoursRestTime === 0 && selectedMinutesRestTime === 0 && selectedSecRestTime === 0) {
            alert("Rest Time can't be zero!")
        } else if (totalRounds === "" || Number.parseInt(totalRounds) < 1) {
            alert("Total rounds can't be empty or zero!")
        } else {
            if (this.props.route.params.addFlag === true) {
                var intervalObj = {

                    name: this.state.workoutName,
                    deletedAble: true,
                    rounds: this.state.totalRounds,
                    workTime: {
                        hr: this.state.selectedHoursWorkTime,
                        min: this.state.selectedMinutesWorkTime,
                        sec: this.state.selectedSecWorkTime,
                    },
                    restTime: {
                        hr: this.state.selectedHoursRestTime,
                        min: this.state.selectedMinutesRestTime,
                        sec: this.state.selectedSecRestTime,
                    }
                };
                this.props.route.params.callBackCustomIntervalAdded(intervalObj);
                this.props.navigation.goBack(null);

            } else {
                var intervalObj = {
                    name: this.state.workoutName,
                    deletedAble: this.props.route.params.item.deletedAble,
                    rounds: this.state.totalRounds,
                    workTime: {
                        hr: this.state.selectedHoursWorkTime,
                        min: this.state.selectedMinutesWorkTime,
                        sec: this.state.selectedSecWorkTime,
                    },
                    restTime: {
                        hr: this.state.selectedHoursRestTime,
                        min: this.state.selectedMinutesRestTime,
                        sec: this.state.selectedSecRestTime,
                    }
                };

                this.props.route.params.callBackCustomIntervalEdit(intervalObj, this.props.route.params.selectedIndex);
                this.props.navigation.goBack(null);
            }
        }

    };
    handleworkoutTimePicked = (date) => {
        console.log('A date has been picked: ', date);
        this.hideWorkoutTimePicker();
        this.setState({
            workoutTime: Moment(date).format('HH:mm:ss'),
        });
    };
    //REMARK: - CallBacks Time Picker

    showWorkoutTimePicker = () => {
        this.setState({ isWorkoutTimePickerVisible: true });
    };

    hideWorkoutTimePicker = () => {
        this.setState({ isWorkoutTimePickerVisible: false });
    };

    //REMARK: - Separator Line
    renderSeparator = (styleProps) => {
        return (
            <View style={[{
                height: 0.5,
                backgroundColor: colors.lightGray,
                marginLeft: 0,
                marginRight: 0,
            }, styleProps]} />
        );
    };

    //REMARK: - Text Fields for custom intervals
    renderCustomIntervalTF() {
        const { selectedHours, selectedMinutes } = this.state;
        return (
            <View>

                <View
                    style={{
                        marginTop: 25,
                        marginLeft: 20,
                        marginRight: 20,
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}>
                    <TextFieldWithRoundedBorder
                        placeholderText={strings.workoutName}
                        isUpperTextTitle={strings.workoutName}
                        isPasswordEntry={false}
                        isUpperTextShow={true}
                        isCompulsory={true}
                        onChangeText={(workoutName) => this.setState({ workoutName })}
                        value={this.state.workoutName}
                        maxLenthValue={30}
                        keyboardTypeValue="default"
                        autoCapitaliseValue="words"
                        blurOnSubmitValue={false}
                        onSubmitEditing={(event) => {
                            Keyboard.dismiss()

                        }}
                        returnTypeKeyboard="done"
                    />
                </View>

                {!!this.state.wheelSelectorItems && this.state.wheelSelectorItems.length > 0 &&
                    <View>
                        <View style={{
                            marginTop: 20,
                            marginLeft: 20, flexDirection: 'row'
                        }}>
                            <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 24, fontFamily: "SFProText-Medium" }]}>
                                {strings.workoutTime}
                            </Text>
                            <Text style={{ color: colors.appCompulsoryTextColor }}>{' * '}</Text>
                        </View>

                        <View style={{ flexDirection: "row" }}>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Hours</Text>
                                </View>
                                <Picker style={{ flex: 1, height: 120 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedHoursWorkTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onHoursPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Minutes</Text>
                                </View>
                                <Picker style={{ flex: 1 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedMinutesWorkTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onMinutesPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Seconds</Text>
                                </View>
                                <Picker style={{ flex: 1 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedSecWorkTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onSecondsPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>
                        </View>

                        <View style={{
                            marginTop: 20,
                            marginLeft: 20, flexDirection: 'row'
                        }}>
                            <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 24, fontFamily: "SFProText-Medium" }]}>
                                {strings.restTime}
                            </Text>
                            <Text style={{ color: colors.appCompulsoryTextColor }}>{' * '}</Text>
                        </View>

                        <View style={{ flexDirection: "row" }}>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Hours</Text>
                                </View>
                                <Picker style={{ flex: 1, height: 120 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedHoursRestTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onRestHoursPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Minutes</Text>
                                </View>
                                <Picker style={{ flex: 1 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedMinutesRestTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onRestMinutesPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>

                            <View style={{ flex: 0.33 }}>

                                <View style={{
                                    marginTop: 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}>
                                    <Text style={[{ color: colors.appTextColor }, { color: colors.appTextColor, fontSize: 14, fontFamily: "SFProText-Medium" }]}>Seconds</Text>
                                </View>
                                <Picker style={{ flex: 1 }}
                                    lineColor="#000000" //to set top and bottom line color (Without gradients)
                                    lineGradientColorFrom="#000000" //to set top and bottom starting gradient line color
                                    lineGradientColorTo="#000000" //to set top and bottom ending gradient
                                    selectedValue={this.state.selectedSecRestTime}
                                    itemStyle={{ color: "#000000", fontSize: 26, fontWeight: "bold" }}
                                    onValueChange={(index) => {
                                        this.onRestSecondsPickerSelect(index)
                                    }}>
                                    {this.state.wheelSelectorItems.map((value, i) => (
                                        <PickerItem label={value} value={i} key={i} />
                                    ))}
                                </Picker>
                            </View>
                        </View>
                    </View>
                }

                <View
                    style={{
                        marginTop: 25,
                        marginLeft: 20,
                        marginRight: 20,
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}>
                    <TextFieldWithRoundedBorder
                        // ref={this.totalRoundsInput}
                        placeholderText={strings.totalRounds}
                        isUpperTextTitle={strings.totalRounds}
                        isPasswordEntry={false}
                        isUpperTextShow={true}
                        isCompulsory={true}
                        onChangeText={(totalRounds) => this.setState({ totalRounds })}
                        value={this.state.totalRounds}
                        maxLenthValue={30}
                        keyboardTypeValue="default"
                        autoCapitaliseValue="words"
                        keyboardType="number-pad"
                        blurOnSubmitValue={false}
                        input
                        onSubmitEditing={(event) => {

                            Keyboard.dismiss()
                        }}
                        returnTypeKeyboard="done"
                    />
                </View>

            </View>
        );
    }

    //REMARK: - Submit btn to save or edit View
    renderSubmitBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        marginTop: 40,
                    },
                ]}>
                <ButtonFill
                    btnTitle={strings.save}
                    onPress={this.btnActionSaveInterval.bind(this)}
                />
            </View>
        );

    }
    render() {

        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.commonBackground,

                }}>
                <HeaderCenter
                    titleText={strings.setIntervals}
                    cardStyle={true}
                    leftImageSource={backImage}

                    onPressLeftButton={() => {
                        this.props.navigation.goBack(null);
                    }}
                    leftType="image"

                />
                <ScrollView keyboardShouldPersistTaps="always">

                    {this.renderCustomIntervalTF()}
                    {this.renderSubmitBtnView()}
                </ScrollView>

            </SafeAreaView>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { appName } = state.authReducer;
    return {
        appName,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddAndEditIntervalScreen);