import React, { Component } from "react";
import { Alert, FlatList, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { height as h } from "react-native-dimension";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from "react-redux";
import { strings } from "../../../constants/Localization";
import { ScreenNames } from "../../../constants/ScreenNames";
import colors from "../../../helpers/colors";
import { plusImageBlack } from "../../../helpers/Images";
import {
    AsyncGetViaKey,

    AsyncStoreViaKey
} from "../../../helpers/LocalStorage/AsyncStorage";
import { navigateWithParams, showToast } from "../../../helpers/Util";


class IntervalScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            intervalModeList: [],
        };

    }

    componentDidMount() {
        this.getDataFromDb();
    }

    updateLocalDB(newArray) {
        AsyncStoreViaKey("key1", newArray).then((obj) => {

            if (!!obj) {
                // alert("Success!")
                showToast("Success")
            } else {
                showToast("Error")
                // alert("Error!")
            }

        });
    }

    getDataFromDb() {


        AsyncGetViaKey("key1").then((obj) => {
            if (!!obj) {

                this.setState({ intervalModeList: obj })
            } else {
                // alert("Error");
            }

        });

    }
    //REMARK: - Actions
    btnActionAddIntverals = () => {

        navigateWithParams(this.props.navigation, ScreenNames.AddAndEditIntervalScreen, {
            intervalList: this.state.intervalModeList,
            addFlag: true,
            callBackCustomIntervalAdded: this.callBackCustomIntervalAdded,
        });
    };

    btnDeletedIsPressed = (item, index) => {

        var newData = this.state.intervalModeList;
        newData.splice(index, 1);

        this.setState({
            intervalModeList: newData
        });

        this.updateLocalDB(newData);
    };

    btnActionDeleteIntervalMode = (item, index) => {

        if (item.deletedAble === true) {
            Alert.alert(
                '',
                'Are you sure you want delete this item',
                [
                    { text: strings.cancel, onPress: () => console.log('Cancel Pressed!') },
                    { text: strings.ok, onPress: this.btnDeletedIsPressed.bind(this, item, index) },
                ],
                { cancelable: false }
            )
        } else {
            alert("You can't delete pre defined intervals!");
        }

    };

    btnActionEditIntervalMode = (item, index) => {

        navigateWithParams(this.props.navigation, ScreenNames.AddAndEditIntervalScreen, {
            item: item,
            selectedIndex: index,
            addFlag: false,
            callBackCustomIntervalEdit: this.callBackCustomIntervalEdit,
        });
    };

    btnActionIntervalDetail = (item, index) => {
        navigateWithParams(this.props.navigation, ScreenNames.DetailIntervalMode, { item: item });
    };
    //RMEARK: - CallBacks
    callBackCustomIntervalAdded = (item) => {

        var newData = this.state.intervalModeList;
        newData.push(item);

        this.setState({
            intervalModeList: newData
        })

        this.updateLocalDB(newData);
    }

    callBackCustomIntervalEdit = (item, index) => {

        var newData = this.state.intervalModeList;
        newData[index] = item;

        this.setState({
            intervalModeList: newData
        })

        this.updateLocalDB(newData);
    }
    //REMARK: - Separator Line
    renderSeparator = (styleProps) => {
        return (
            <View style={[{
                height: 0.3,
                backgroundColor: colors.lightGray,
                marginLeft: 0,
                marginRight: 0,
            }, styleProps]} />
        );
    };
    //REMARK: - Different Modes Interval Flat List
    renderIntervalModeFlatList() {

        return (
            <View style={{
                marginTop: 10,
                marginLeft: 10,
                marginRight: 10,
                backgroundColor: colors.intervalCellBackground
            }}>
                <FlatList
                    data={this.state.intervalModeList}
                    horizontal={false}

                    renderItem={({ item, index }) =>
                        this.renderCellIntervalModeItem(item, index)

                    }
                    // ItemSeparatorComponent={this.renderSeparator}
                    keyExtractor={(item, index) => item}
                    keyboardShouldPersistTaps="always"
                />
            </View>
        );
    }
    renderCellIntervalModeItem = (item, index) => {
        return (
            <View style={{
                justifyContent: 'center',
                marginLeft: 10,
                // alignItems: 'center'
            }}>

                <TouchableOpacity
                    onPress={this.btnActionIntervalDetail.bind(this, item, index)}
                >

                    <View
                        style={{
                            marginTop: 10,
                            flexDirection: 'row',
                            width: '100%',
                            // marginHorizontal: '5%',
                            // marginBottom: 10,
                        }}

                    >

                        <View style={{
                            width: '70%',
                            // marginLeft: 10,
                        }}>
                            <Text
                                style={[
                                    {
                                        textAlign: 'left', fontSize: 18,
                                        color: colors.appTextColor,
                                        fontFamily: "SFProText-Medium"
                                    },

                                ]}
                                numberOfLines={1}>
                                {item.name}
                            </Text>


                        </View>

                        <TouchableOpacity
                            style={{ width: '15%', justifyContent: 'center', alignItems: 'center' }}
                            onPress={this.btnActionEditIntervalMode.bind(this, item, index)}
                        >
                            <Ionicons name={'ios-pencil-sharp'} size={h(3)} color={colors.appSelectedImageColor} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ width: '15%', justifyContent: 'flex-end', }}
                            onPress={this.btnActionDeleteIntervalMode.bind(this, item, index)}
                        >
                            <Ionicons name={'trash-outline'} size={h(3)} color={colors.appSelectedImageColor} />
                        </TouchableOpacity>

                    </View>
                </TouchableOpacity>
                {this.renderSeparator({
                    left: 0,
                    marginTop: 15,
                })}
            </View>
        );
    }

    render() {

        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.commonBackground,

                }}>

                <ScrollView keyboardShouldPersistTaps="always">

                    <TouchableOpacity
                        style={{
                            marginTop: 10,
                            padding: 10,
                            alignSelf: 'flex-end',
                        }}
                        onPress={this.btnActionAddIntverals}>

                        <Image
                            style={[
                                {
                                    width: 24,
                                    height: 24,
                                    paddingBottom: 10,
                                    paddingTop: 10,
                                    paddingRight: 10,
                                    marginRight: 10,
                                    tintColor: colors.appImageColor,
                                },
                            ]}
                            source={plusImageBlack}
                        />

                    </TouchableOpacity>
                    <View >

                        <Text style={{
                            textAlign: 'left', color: colors.appTextColor,
                            fontSize: 22,
                            marginLeft: 20,
                            fontFamily: "SFProText-Medium"
                        }}>
                            {strings.intervalMode}
                        </Text>

                    </View>
                    <View >

                        <Text style={{
                            textAlign: 'left', color: colors.appTextColor,
                            fontSize: 16,
                            marginLeft: 20,
                            marginTop: 40,
                            fontFamily: "SFProText-Medium"
                        }}>{strings.customIntervalMode}</Text>

                    </View>
                    {this.renderIntervalModeFlatList()}
                </ScrollView>

            </SafeAreaView>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { appName } = state.authReducer;
    return {
        appName,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IntervalScreen);