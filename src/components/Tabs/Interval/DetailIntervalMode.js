import React, { Component } from "react";
import {
    Dimensions, Platform, SafeAreaView, ScrollView, Text, View
} from "react-native";
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { connect } from "react-redux";
import { strings } from "../../../constants/Localization";
import ButtonFill from '../../../CustomComponents/ButtomFill.js';
import HeaderCenter from "../../../CustomComponents/Header/HeaderCenter";
import colors from "../../../helpers/colors";
import { counterModeEnum, intervalRoundModeEnum, stopWatchEnum } from "../../../helpers/enum";
import { backImage } from "../../../helpers/Images";
import { playSound } from "../../../helpers/soundUtil";


const screenWidth = Dimensions.get('window').width;

var lastPlayedSoundNumber = 3;

class DetailIntervalMode extends Component {

    constructor(props) {
        super(props);

        this.state = {

            firstTimeInitiated: true,
            key: 0,

            status: stopWatchEnum.INITIAL,
            roundMode: intervalRoundModeEnum.WORKOUT,

            maxDuration: 60 * 15,

            currentDuration: 0,

            workoutName: '',

            currentRound: 0,
            totalRounds: 0,

            selectedHoursWorkTime: 0,
            selectedMinutesWorkTime: 0,
            selectedSecWorkTime: 0,

            selectedHoursRestTime: 0,
            selectedMinutesRestTime: 0,
            selectedSecRestTime: 0,

            mode: counterModeEnum.COUNT_UP,

        };
    }

    componentDidMount() {

        if (!!this.props.route.params.item) {
            this.setState({
                workoutName: this.props.route.params.item.name,
                totalRounds: this.props.route.params.item.rounds,

                selectedHoursWorkTime: this.props.route.params.item.workTime.hr,
                selectedMinutesWorkTime: this.props.route.params.item.workTime.min,
                selectedSecWorkTime: this.props.route.params.item.workTime.sec,

                selectedHoursRestTime: this.props.route.params.item.restTime.hr,
                selectedMinutesRestTime: this.props.route.params.item.restTime.min,
                selectedSecRestTime: this.props.route.params.item.restTime.sec,

                currentDuration: this.props.route.params.item.workTime.hr * 3600 + this.props.route.params.item.workTime.min * 60 + this.props.route.params.item.workTime.sec
            })
        }
    }

    //REMARK: - Actions
    btnActionStart = () => {

        this.setState({

            key: 1,
            status: stopWatchEnum.START,

            currentRound: this.state.currentRound + 1,
            currentDuration: this.state.selectedHoursWorkTime * 3600 + this.state.selectedMinutesWorkTime * 60 + this.state.selectedSecWorkTime,
            roundMode: intervalRoundModeEnum.WORKOUT
        });
    };
    btnActionStop = () => {

        this.setState({
            status: stopWatchEnum.STOP,
        });
    };
    btnActionReset = () => {

        this.setState({
            status: stopWatchEnum.RESET,
            key: this.state.key + 1,

            currentRound: 0,
            currentDuration: this.state.selectedHoursWorkTime * 3600 + this.state.selectedMinutesWorkTime * 60 + this.state.selectedSecWorkTime,
            roundMode: intervalRoundModeEnum.WORKOUT
        });
    };
    btnActionResume = () => {

        this.setState({
            status: stopWatchEnum.RESUME
        });
    };

    autoActionNext = () => {

        if (this.state.roundMode === intervalRoundModeEnum.WORKOUT && this.state.currentRound === Number.parseInt(this.state.totalRounds)) {

            this.btnActionReset();
            playSound("beep2", "mp3");

        } else {
            this.setState({
                status: stopWatchEnum.START,
                key: this.state.key + 1,

                currentRound: (this.state.roundMode === intervalRoundModeEnum.REST) ? (this.state.currentRound + 1) : (this.state.currentRound),
                currentDuration: (this.state.roundMode === intervalRoundModeEnum.REST) ? (this.state.selectedHoursWorkTime * 3600 + this.state.selectedMinutesWorkTime * 60 + this.state.selectedSecWorkTime) : (this.state.selectedHoursRestTime * 3600 + this.state.selectedMinutesRestTime * 60 + this.state.selectedSecRestTime),
                roundMode: (this.state.roundMode === intervalRoundModeEnum.REST) ? (intervalRoundModeEnum.WORKOUT) : (intervalRoundModeEnum.REST)
            });
        }

    };

    btnActionCountUp = () => {
        this.setState({
            mode: counterModeEnum.COUNT_UP,
            currentDuration: this.state.selectedHoursWorkTime * 3600 + this.state.selectedMinutesWorkTime * 60 + this.state.selectedSecWorkTime,
        });
    };
    btnActionCountDown = () => {
        this.setState({
            mode: counterModeEnum.COUNT_DOWN,
            currentDuration: this.state.selectedHoursWorkTime * 3600 + this.state.selectedMinutesWorkTime * 60 + this.state.selectedSecWorkTime,
        });
    };

    formatRemainingTime(time) {
        console.log('${time}:', time)
        const minutes = Math.floor((time % 3600) / 60);
        const seconds = time % 60;
        console.log('${minutes}:', minutes, '${seconds}:', seconds)
        return `${minutes} : ${seconds}`;
    };
    renderTime(remainingTime, isPlaying) {

        if (remainingTime === 0) {
            lastPlayedSoundNumber = 3
        } else if (remainingTime <= lastPlayedSoundNumber && isPlaying) {

            if (lastPlayedSoundNumber - remainingTime === 0) {
                playSound("beep1", "mp3");
            }

            if (lastPlayedSoundNumber > 0) {
                lastPlayedSoundNumber = lastPlayedSoundNumber - 1
            } else {

            }
        }

        return (
            <View>

                <Text style={{
                    textAlign: 'center', color: (this.state.roundMode === intervalRoundModeEnum.WORKOUT) ? (colors.appTextColor) : (colors.appSelectedTextColor),
                    fontFamily: (Platform.OS === 'android') ? ('digital-7 (italic)') : ('Digital-7Italic'),
                    fontSize: 60,
                }}>{this.formatRemainingTime(remainingTime)}</Text>

            </View>

        );
    };

    //REMARK: - Reset And Resume Btn View
    renderResumeAndResetBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        // width: screenWidth - 40,
                        flexDirection: 'row',
                    },
                ]}>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,
                            marginRight: 10,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.reset}
                        style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                        btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                        onPress={this.btnActionReset.bind(this)}
                    />
                </View>
                <View
                    style={[
                        {
                            width: screenWidth / 2 - 25,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.resume}

                        onPress={this.btnActionResume.bind(this)}
                    />
                </View>
            </View>

        );
    }
    //REMARK: - CountUp And CountDown Btn View
    renderCountUpAndCountDownBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        // width: screenWidth - 40,
                        flexDirection: 'row',
                    },
                ]}>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,
                            marginRight: 10,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.countUp}
                        style={{ backgroundColor: (this.state.mode === counterModeEnum.COUNT_UP) ? (colors.redColor) : (colors.darkGray) }}
                        onPress={this.btnActionCountUp.bind(this)}
                    />
                </View>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,

                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.countDown}
                        style={{ backgroundColor: (this.state.mode === counterModeEnum.COUNT_DOWN) ? (colors.redColor) : (colors.darkGray) }}
                        onPress={this.btnActionCountDown.bind(this)}
                    />
                </View>
            </View>

        );
    }

    render() {
        var playTimer = false
        if (this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME) {
            playTimer = true;
        }

        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.commonBackground,

                }}>

                <HeaderCenter
                    titleText={this.state.workoutName}
                    cardStyle={true}
                    leftImageSource={backImage}

                    onPressLeftButton={() => {
                        this.props.navigation.goBack(null);
                    }}
                    leftType="image"

                />
                <ScrollView keyboardShouldPersistTaps="always">

                    <View style={{ alignItems: 'center', marginTop: 100, marginLeft: 20, marginRight: 20 }}>

                        <CountdownCircleTimer
                            key={this.state.key}
                            isPlaying={playTimer}
                            duration={this.state.currentDuration}
                            trailColor={(this.state.roundMode === intervalRoundModeEnum.WORKOUT) ? (colors.appCircularTimeColor) : (colors.appCircularRestTimeColor)}
                            colors={colors.appCircularTimeSecondaryColor}
                            size={300}
                            onComplete={() => {

                                this.autoActionNext();

                                return [false, 0] // repeat animation in 1.5 seconds
                            }}
                        >
                            {({ remainingTime, animatedColor }) => (
                                this.renderTime(remainingTime, playTimer)
                            )}
                        </CountdownCircleTimer>
                    </View>

                    <View style={{ width: "100%", justifyContent: "center", alignItems: "center", marginTop: 10 }}>

                        <Text style={{ color: colors.appTextColor, fontSize: 25, fontFamily: "SFProText-Medium" }}>ROUNDS</Text>
                        <View style={{ flexDirection: "row", marginTop: 10, justifyContent: "flex-end", alignItems: "flex-end" }}>
                            <Text style={{ fontFamily: "SFProText-Medium", color: (this.state.roundMode === intervalRoundModeEnum.WORKOUT) ? (colors.appCircularTimeColor) : (colors.appCircularRestTimeColor), fontSize: 28 }}>{this.state.currentRound}</Text>
                            <Text style={{ fontFamily: "SFProText-Medium", color: colors.appTextColor, fontSize: 20 }}>/{this.state.totalRounds}</Text>
                        </View>

                    </View>
                </ScrollView>

                {this.state.status === stopWatchEnum.INITIAL || this.state.status === stopWatchEnum.RESET ?
                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20
                                // width: screenWidth - 40,
                            },
                        ]}>
                        <ButtonFill
                            btnTitle={strings.start}

                            onPress={this.btnActionStart.bind(this)}
                        />
                    </View> : null}
                {this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME ?
                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20
                                // width: screenWidth - 40,
                            },
                        ]}>
                        <ButtonFill
                            btnTitle={strings.stop}
                            style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                            btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                            onPress={this.btnActionStop.bind(this)}
                        />
                    </View> : null}

                {this.state.status === stopWatchEnum.STOP ? this.renderResumeAndResetBtnView() : null}
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { appName } = state.authReducer;
    return {
        appName,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailIntervalMode);