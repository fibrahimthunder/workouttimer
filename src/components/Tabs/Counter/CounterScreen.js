import React, { Component } from "react";
import {
    Dimensions, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View
} from "react-native";
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { connect } from "react-redux";
import { strings } from "../../../constants/Localization";
import ButtonFill from '../../../CustomComponents/ButtomFill.js';
import colors from "../../../helpers/colors";
import { counterModeEnum, stopWatchEnum } from "../../../helpers/enum";
import { playSound } from "../../../helpers/soundUtil";
import TimePickerDialog from "../../../helpers/TimePickerDialog";

const screenWidth = Dimensions.get('window').width;

var lastPlayedSoundNumber = 3;

class CounterScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            key: 1,
            status: stopWatchEnum.INITIAL,
            mode: counterModeEnum.COUNT_UP,
            maxDuration: 60 * 0,

            showPicker: false,
        };
    }

    componentDidMount() {

    }

    //REMARK: - Actions
    btnActionStart = () => {
        this.setState({
            status: stopWatchEnum.START
        });
    };
    btnActionStop = () => {
        this.setState({
            status: stopWatchEnum.STOP
        });
    };
    btnActionReset = () => {
        this.setState({
            status: stopWatchEnum.RESET,
            key: this.state.key + 1
        });
    };
    btnActionResume = () => {
        this.setState({
            status: stopWatchEnum.RESUME
        });
    };

    btnActionCountUp = () => {
        this.setState({
            mode: counterModeEnum.COUNT_UP
        });
    };
    btnActionCountDown = () => {
        this.setState({
            mode: counterModeEnum.COUNT_DOWN
        });
    };

    timeUpdated = newTime => {


        this.setState({
            key: this.state.key + 1,
            showPicker: false,
            maxDuration: newTime.hrs * 3600 + newTime.mins * 60 + newTime.secs
        });
    };

    formatRemainingTime(time) {

        const hours = Math.floor((time / 3600));
        const minutes = Math.floor((time % 3600) / 60);
        const seconds = time % 60;

        if (hours > 0) {
            return `${hours} : ${minutes} : ${seconds}`;
        } else {
            return `${minutes} : ${seconds}`;
        }

    };
    renderTime(remainingTime, isPlaying) {

        var t = (this.state.mode === counterModeEnum.COUNT_UP) ? (this.state.maxDuration - remainingTime) : (remainingTime);


        if (remainingTime === 0) {
            lastPlayedSoundNumber = 3
        } else if (remainingTime <= lastPlayedSoundNumber && isPlaying) {


            if (lastPlayedSoundNumber - remainingTime === 0) {
                playSound("beep1", "mp3");
                console.log("Fahad Time: ", remainingTime);
            }

            if (lastPlayedSoundNumber > 0) {

                lastPlayedSoundNumber = lastPlayedSoundNumber - 1
            } else {

            }
        }
        return (

            <TouchableOpacity
                style={{ justifyContent: "center", alignSelf: "center", alignItems: "center", fontFamily: "SFProText-Medium" }}
                onPress={() => {

                    if (this.state.status === stopWatchEnum.INITIAL || this.state.status === stopWatchEnum.RESET) {
                        this.setState({ showPicker: true });
                    }

                }}>
                <View>

                    <Text style={{
                        textAlign: 'center',
                        color: colors.appTextColor,
                        fontFamily: (Platform.OS === 'android') ? ('digital-7 (italic)') : ('Digital-7Italic'),
                        fontSize: 60,

                    }}>{this.formatRemainingTime(t)}</Text>

                </View>

            </TouchableOpacity>

        );
    };

    //REMARK: - Reset And Resume Btn View
    renderResumeAndResetBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        flexDirection: 'row',
                    },
                ]}>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,
                            marginRight: 10,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.reset}
                        style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                        btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                        onPress={this.btnActionReset.bind(this)}
                    />
                </View>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,

                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.resume}
                        onPress={this.btnActionResume.bind(this)}
                    />
                </View>
            </View>

        );
    }
    //REMARK: - CountUp And CountDown Btn View
    renderCountUpAndCountDownBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        // width: screenWidth - 40,
                        flexDirection: 'row',
                    },
                ]}>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,
                            marginRight: 10,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.countUp}
                        style={{ backgroundColor: (this.state.mode === counterModeEnum.COUNT_UP) ? (colors.appButtonSelectedBackgroundColor) : (colors.appButtonDisabledBackgroundColor) }}
                        btnStyle={{ color: (this.state.mode === counterModeEnum.COUNT_UP) ? (colors.appButtonSelectedTextColor) : (colors.appButtonDisabledTextColor) }}
                        onPress={this.btnActionCountUp.bind(this)}
                    />
                </View>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,

                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.countDown}
                        style={{ backgroundColor: (this.state.mode === counterModeEnum.COUNT_DOWN) ? (colors.appButtonSelectedBackgroundColor) : (colors.appButtonDisabledBackgroundColor) }}
                        btnStyle={{ color: (this.state.mode === counterModeEnum.COUNT_DOWN) ? (colors.appButtonSelectedTextColor) : (colors.appButtonDisabledTextColor) }}
                        onPress={this.btnActionCountDown.bind(this)}
                    />
                </View>
            </View>

        );
    }

    render() {

        var playTimer = false
        if (this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME) {
            playTimer = true;
        }

        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.commonBackground,

                }}>

                <TimePickerDialog
                    show={this.state.showPicker}
                    onSubmitTime={this.timeUpdated}
                />
                <ScrollView keyboardShouldPersistTaps="always">

                    <View style={{ alignItems: 'center', marginTop: 100, marginLeft: 20, marginRight: 20 }}>

                        <CountdownCircleTimer
                            key={this.state.key}
                            isPlaying={playTimer}
                            duration={this.state.maxDuration}
                            trailColor={colors.appCircularTimeColor}
                            colors={colors.appCircularTimeSecondaryColor}
                            size={300}
                            onComplete={() => {

                                playSound("beep2", "mp3");
                                this.btnActionReset()
                                return [false, 0] // repeat animation in 1.5 seconds
                            }}
                        >

                            {({ remainingTime, animatedColor }) => (

                                this.renderTime(remainingTime, playTimer)
                            )}
                        </CountdownCircleTimer>
                    </View>
                </ScrollView>

                {this.state.status === stopWatchEnum.INITIAL || this.state.status === stopWatchEnum.RESET ? this.renderCountUpAndCountDownBtnView() : null}

                {this.state.status === stopWatchEnum.INITIAL || this.state.status === stopWatchEnum.RESET ?
                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20
                            },
                        ]}>
                        <ButtonFill
                            btnTitle={strings.start}
                            onPress={this.btnActionStart.bind(this)}
                        />
                    </View> : null}
                {this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME ?
                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20
                            },
                        ]}>
                        <ButtonFill
                            btnTitle={strings.stop}
                            style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                            btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                            onPress={this.btnActionStop.bind(this)}
                        />
                    </View> : null}

                {this.state.status === stopWatchEnum.STOP ? this.renderResumeAndResetBtnView() : null}

            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { appName } = state.authReducer;
    return {
        appName,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CounterScreen);