import React, { Component } from "react";
import {
    Dimensions, FlatList, Platform, SafeAreaView, ScrollView, Text, View
} from "react-native";
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { connect } from "react-redux";
import { strings } from "../../../constants/Localization";
import ButtonFill from '../../../CustomComponents/ButtomFill.js';
import colors from "../../../helpers/colors";
import { stopWatchEnum } from "../../../helpers/enum";
import { playSound } from "../../../helpers/soundUtil";

const screenWidth = Dimensions.get('window').width;

const _keyExtractor = (item, index) => index.toString();

class StopwatchScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            key: 1,
            status: stopWatchEnum.INITIAL,

            maxDuration: 3600 * 24,

            showPicker: false,
            clock: 0,

            lapData: [],

            lapNo: 0,
            lastLapTime: 0,
        };
    }

    componentDidMount() {

    }

    //REMARK: - Actions
    btnActionStart = () => {

        this.setState({
            status: stopWatchEnum.START
        });
    };
    btnActionStop = () => {

        this.setState({
            status: stopWatchEnum.STOP
        });
    };
    btnActionReset = () => {

        this.setState({
            status: stopWatchEnum.RESET,
            key: this.state.key + 1,

            lapNo: 0,
            lastLapTime: 0,
            lapData: []
        });
    };
    btnActionResume = () => {

        this.setState({
            status: stopWatchEnum.RESUME
        });
    };


    btnActionLAP = () => {

        playSound("beep1", "mp3");
        var lapObj = {
            lapNo: this.state.lapNo + 1,
            lapTime: this.formatRemainingTime(this.state.clock),
            lapSplit: this.formatRemainingTime(this.state.clock - this.state.lastLapTime)
        }

        var oldLapData = this.state.lapData;

        var newLapData = [lapObj].concat(oldLapData)

        this.setState({
            lapNo: this.state.lapNo + 1,
            lapData: newLapData,
            lastLapTime: this.state.clock
        });
    };

    timeUpdated = newTime => {


        this.setState({
            key: this.state.key + 1,
            showPicker: false,
            maxDuration: newTime.hrs * 3600 + newTime.mins * 60 + newTime.secs
        });
    };

    formatRemainingTime(time) {

        const hours = Math.floor((time / 3600));
        const minutes = Math.floor((time % 3600) / 60);
        const seconds = time % 60;

        if (hours > 0) {
            return `${hours} : ${minutes} : ${seconds}`;
        } else {
            return `${minutes} : ${seconds}`;
        }
    };
    renderTime(remainingTime) {

        if (this.state.clock !== remainingTime) {

            const hours = Math.floor((remainingTime / 3600));

            this.setState({
                clock: remainingTime,
                currentHours: hours
            })
        }

        return (

            <View>

                <Text style={{
                    textAlign: 'center', color: colors.appTextColor,
                    fontFamily: (Platform.OS === 'android') ? ('digital-7 (italic)') : ('Digital-7Italic'),
                    fontSize: 60,
                }}>{this.formatRemainingTime(remainingTime)}</Text>


            </View>

        );
    };

    //REMARK: - Reset And Resume Btn View
    renderResumeAndResetBtnView() {
        return (
            <View
                style={[
                    {
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        // width: screenWidth - 40,
                        flexDirection: 'row',
                    },
                ]}>
                <View
                    style={[
                        {

                            width: screenWidth / 2 - 25,
                            marginRight: 10,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.reset}
                        style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                        btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                        onPress={this.btnActionReset.bind(this)}
                    />
                </View>
                <View
                    style={[
                        {
                            width: screenWidth / 2 - 25,
                        },
                    ]}
                >
                    <ButtonFill
                        btnTitle={strings.resume}
                        onPress={this.btnActionResume.bind(this)}
                    />
                </View>
            </View>

        );
    }

    renderIntervalModeFlatList() {

        return (
            <View style={{
                marginTop: 40,
                marginLeft: 10,
                marginRight: 10,
                marginBottom: 30
                // backgroundColor: colors.intervalCellBackground
            }}>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>Lap No</Text>
                    </View>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>Time</Text>
                    </View>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>Split</Text>
                    </View>
                </View>
                <FlatList
                    style={{ width: "100%", height: 300 }}
                    data={this.state.lapData}
                    horizontal={false}
                    keyExtractor={_keyExtractor}
                    renderItem={({ item, index }) =>
                        this.renderCellIntervalModeItem(item, index)
                    }
                    // ItemSeparatorComponent={this.renderSeparator}
                    keyExtractor={(item, index) => item}
                    keyboardShouldPersistTaps="always"
                />
            </View>
        );
    }

    renderCellIntervalModeItem = (item, index) => {
        return (
            <View style={{
                justifyContent: 'center',
                marginTop: 20
                // alignItems: 'center'
            }}
                key={index}>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>{item.lapNo}</Text>
                    </View>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>{item.lapTime}</Text>
                    </View>
                    <View style={{ flex: 0.33, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[{ color: colors.appTextColor, fontSize: 18, fontFamily: "SFProText-Medium" }]}>{item.lapSplit}</Text>
                    </View>
                </View>
                <View style={{
                    height: 0.3,
                    backgroundColor: colors.lightGray,
                    marginLeft: 10,
                    marginRight: 0,
                    left: 0,
                    marginTop: 15,
                }} />
            </View>
        );
    }
    render() {
        var playTimer = false
        if (this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME) {
            playTimer = true;
        }

        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: colors.commonBackground,

                }}>

                <ScrollView
                    style={{ marginBottom: 30 }}
                    keyboardShouldPersistTaps="always">

                    <View style={{ alignItems: 'center', marginTop: 50, marginLeft: 20, marginRight: 20 }}>
                        <Text style={{
                            textAlign: 'center', color: colors.appTextColor,
                            fontFamily: (Platform.OS === 'android') ? ('digital-7 (italic)') : ('Digital-7Italic'),
                            fontSize: 90,


                        }}>{this.formatRemainingTime(this.state.clock)}</Text>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 100, marginLeft: 20, marginRight: 20, display: "none" }}>

                        <CountdownCircleTimer
                            key={this.state.key}
                            isPlaying={playTimer}
                            duration={this.state.maxDuration}
                            trailColor={colors.appCircularTimeColor}
                            colors={colors.appCircularTimeSecondaryColor}
                            size={300}
                            onComplete={() => {

                                this.btnActionReset()
                                return [false, 0] // repeat animation in 1.5 seconds
                            }}
                        >

                            {({ remainingTime, animatedColor }) => (

                                this.renderTime(this.state.maxDuration - remainingTime)
                            )}
                        </CountdownCircleTimer>
                    </View>

                    {this.renderIntervalModeFlatList()}
                </ScrollView>
                {this.state.status === stopWatchEnum.INITIAL || this.state.status === stopWatchEnum.RESET ?
                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20
                                // width: screenWidth - 40,
                            },
                        ]}>
                        <ButtonFill
                            btnTitle={strings.start}

                            onPress={this.btnActionStart.bind(this)}
                        />
                    </View> : null}
                {this.state.status === stopWatchEnum.START || this.state.status === stopWatchEnum.RESUME ?

                    <View
                        style={[
                            {
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20,
                                // width: screenWidth - 40,
                                flexDirection: 'row',
                            },
                        ]}>
                        <View
                            style={[
                                {

                                    width: screenWidth / 2 - 25,
                                    marginRight: 10,
                                },
                            ]}
                        >
                            <ButtonFill
                                btnTitle={strings.lap}

                                onPress={this.btnActionLAP.bind(this)}
                            />
                        </View>

                        <View
                            style={[
                                {
                                    width: screenWidth / 2 - 25,
                                },
                            ]}>
                            <ButtonFill
                                btnTitle={strings.stop}
                                style={{ backgroundColor: colors.appButtonUnSelectedBackgroundColor }}
                                btnStyle={{ color: colors.appButtonUnSelectedTextColor }}
                                onPress={this.btnActionStop.bind(this)}
                            />
                        </View>
                    </View> : null}

                {this.state.status === stopWatchEnum.STOP ? this.renderResumeAndResetBtnView() : null}


            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const { appName } = state.authReducer;
    return {
        appName,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StopwatchScreen);