import { SET_APP_STATE } from "../../constants/ReducerEnums";

const initialState = {

    appName: "Workout Timer",
    appState: "GUEST"
}

export default function authReducer(state = initialState, action) {
    switch (action.type) {

        case SET_APP_STATE:
            {
                return {
                    ...state,// current state
                    appState: action.payload.appState
                }
            }

        default:
            return state;
    }
}