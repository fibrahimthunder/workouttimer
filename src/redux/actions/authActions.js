import { SET_APP_STATE } from "../../constants/ReducerEnums";

export const setAppState = obj => ({
  type: SET_APP_STATE,
  payload: obj
});