export const stopWatchEnum = {
    INITIAL: 'INITIAL',
    START: 'START',
    STOP: 'STOP',
    RESUME: 'RESUME',
    RESET: 'RESET'
};

export const counterModeEnum = {
    COUNT_UP: 'COUNT_UP',
    COUNT_DOWN: 'COUNT_DOWN',
};

export const intervalRoundModeEnum = {
    WORKOUT: 'WORKOUT',
    REST: 'REST',
};