const Logo = require("../assets/images/logo.png");
const plusImageBlack = require("../assets/images/plusImageBlack.png");
const edit = require("../assets/images/edit.png");
const deleteIcon = require("../assets/images/delete_icon.png");
const backImage = require("../assets/images/backImage.png");
const backImageBlack = require("../assets/images/backImageBlack.png");
const splashScreen = require("../assets/images/splashScreen.png");
export {
    Logo,
    splashScreen,
    plusImageBlack,
    deleteIcon,
    edit,
    backImage,
    backImageBlack
}