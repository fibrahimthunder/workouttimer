import SoundPlayer from 'react-native-sound-player'

  _onFinishedPlayingSubscription = null
  _onFinishedLoadingSubscription = null
  _onFinishedLoadingFileSubscription = null
  _onFinishedLoadingURLSubscription = null

  export function registerEvents() {
    _onFinishedPlayingSubscription = SoundPlayer.addEventListener('FinishedPlaying', ({ success }) => {
      console.log('finished playing', success)
    })
    _onFinishedLoadingSubscription = SoundPlayer.addEventListener('FinishedLoading', ({ success }) => {
      console.log('finished loading', success)
    })
    _onFinishedLoadingFileSubscription = SoundPlayer.addEventListener('FinishedLoadingFile', ({ success, name, type }) => {
      console.log('finished loading file', success, name, type)
    })
    _onFinishedLoadingURLSubscription = SoundPlayer.addEventListener('FinishedLoadingURL', ({ success, url }) => {
      console.log('finished loading url', success, url)
    })
  }

  export function unRegisterEvents() {

    if(!!_onFinishedPlayingSubscription) {
      _onFinishedPlayingSubscription.remove()
      _onFinishedLoadingSubscription.remove()
      _onFinishedLoadingURLSubscription.remove()
      _onFinishedLoadingFileSubscription.remove()
    }
  }

export function playSound(fileName, type) {
    try {
          SoundPlayer.playSoundFile(fileName, type)
      } catch (e) {
        console.log(`cannot play the sound file`, e)
      }
  }