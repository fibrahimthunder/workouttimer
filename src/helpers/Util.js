import Toast from 'react-native-tiny-toast';

export function showToast(title) {
    Toast.show(title);
}

export function navigate(navigation, screenName) {
    navigation.navigate(screenName);
}

export function navigateWithParams(navigation, screenName, params) {
    navigation.navigate(screenName, params);
}

export function getNavPrams(props) {
    return props.navigation.state.params || {};
}
export function push(navigation, screenName) {
    navigation.navigate(screenName);
}

export function pushWithParams(navigation, screenName, params) {
    navigation.push(screenName, params);
}
export function goBack(navigation) {
    navigation.goBack();
}