import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { ScreenNames } from '../constants/ScreenNames';
import { Screens } from './Screens';

const Stack = createStackNavigator();

export default function RootNavigator() {
  return (
    <Stack.Navigator>

      <Stack.Screen name={ScreenNames.SplashScreen} component={Screens.SplashScreen} options={{ headerShown: false }} />
      <Stack.Screen name={ScreenNames.HomeScreen} component={Screens.HomeScreen} />

      <Stack.Screen name={ScreenNames.BottomNavigator} component={Screens.BottomNavigator} options={{ headerShown: false }} />

      <Stack.Screen name={ScreenNames.AddAndEditIntervalScreen} component={Screens.AddAndEditIntervalScreen} options={{ headerShown: false }} />
      <Stack.Screen name={ScreenNames.DetailIntervalMode} component={Screens.DetailIntervalMode} options={{ headerShown: false }} />

    </Stack.Navigator>
  );
}