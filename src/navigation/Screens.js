import HomeScreen from "../components/Home/HomeScreen";
import SplashLoading from "../components/SplashLoading/SplashLoading";
import ClockScreen from "../components/Tabs/Clock/ClockScreen";
import CounterScreen from "../components/Tabs/Counter/CounterScreen";
import AddAndEditIntervalScreens from "../components/Tabs/Interval/AddAndEditIntervalScreen";
import DetailIntervalModes from "../components/Tabs/Interval/DetailIntervalMode";
import IntervalScreen from "../components/Tabs/Interval/IntervalScreen";
import StopwatchScreen from "../components/Tabs/Stopwatch/StopwatchScreen";
import BottomNavigator from "./BottomNavigator";

export const Screens = {
    SplashScreen: SplashLoading,

    HomeScreen: HomeScreen,

    BottomNavigator: BottomNavigator,
    ClockScreen: ClockScreen,
    StopwatchScreen: StopwatchScreen,
    CounterScreen: CounterScreen,
    IntervalScreen: IntervalScreen,

    AddAndEditIntervalScreen: AddAndEditIntervalScreens,
    DetailIntervalMode: DetailIntervalModes,

}
