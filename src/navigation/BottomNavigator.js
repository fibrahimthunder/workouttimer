import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { Component, Fragment } from "react";
import { SafeAreaView } from "react-native";
import { height as h, width as w } from "react-native-dimension";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from "react-redux";
import colors from '../helpers/colors';
import { Screens } from "./Screens";

const Tab = createBottomTabNavigator();

class BottomNavigator extends Component {

  render() {

    return (

      <Fragment>
        
        <SafeAreaView style={{ flex: 1, backgroundColor: colors.commonBackground }}>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Clock') {
                  iconName = focused
                    ? 'time-outline'
                    : 'time-outline';
                } else if (route.name === 'Stopwatch') {
                  iconName = focused ? 'stopwatch-outline' : 'stopwatch-outline';
                } else if (route.name === 'Counter') {
                  iconName = focused ? 'speedometer-outline' : 'speedometer-outline';
                } else if (route.name === 'Interval') {
                  iconName = focused ? 'sync-outline' : 'sync-outline';
                }

                // You can return any component that you like here!
                return <Ionicons name={iconName} size={h(4)} color={color} />;
              },
              tabBarVisible: true
            })}
            tabBarOptions={{
              adaptive: false,
              style: { height: h(9) },

              activeTintColor: colors.appSelectedTextColor,
              inactiveTintColor: colors.appTextColor,
              tabStyle: {

                height: h(9),
                paddingBottom: w(1),
                backgroundColor: colors.commonBackground
              },
              labelStyle: {
                fontSize: w(4),
                margin: 0,
                padding: 0,
                fontFamily: "SFProText-Medium"
              },
            }}

          >

            <Tab.Screen name="Clock" component={Screens.ClockScreen} />
            <Tab.Screen name="Stopwatch" component={Screens.StopwatchScreen} />
            <Tab.Screen name="Counter" component={Screens.CounterScreen} />
            <Tab.Screen name="Interval" component={Screens.IntervalScreen} />
          </Tab.Navigator>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {

  return {

  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BottomNavigator);