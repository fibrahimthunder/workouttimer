const DEV = {
    BASE_URL: "https://us-central1-attendanceproject-16d08.cloudfunctions.net/webApi/api",
    TOKEN: ""
}

const PROD = {
    BASE_URL: "https://us-central1-attendanceproject-16d08.cloudfunctions.net/webApi/api",
    TOKEN: ""
}

export const ENV = DEV;

export const API_METHODS = {
    GET: "GET",
    POST: "POST",
    DELETE: "DELETE"
}

export const HEADERS = {
    COMMON: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
}

export const API = {
    SIGNIN_API: `${ENV.BASE_URL}/v1/signin`,
};