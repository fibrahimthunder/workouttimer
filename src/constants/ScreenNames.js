export const ScreenNames = {
    SplashScreen: "SplashScreen",

    HomeScreen: "HomeScreen",

    BottomNavigator: "BottomNavigator",
    ClockScreen: "ClockScreen",
    StopwatchScreen: "StopwatchScreen",
    CounterScreen: "CounterScreen",
    IntervalScreen: "IntervalScreen",
    AddAndEditIntervalScreen: "AddAndEditIntervalScreen",
    DetailIntervalMode: 'DetailIntervalMode',
}