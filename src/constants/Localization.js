
export const strings = {
  intervalMode: 'Interval Modes',
  customIntervalMode: 'CUSTOM INTERVAL MODES',
  setIntervals: 'Set Intervals',
  start: 'START',
  stop: 'STOP',
  resume: 'RESUME',
  reset: 'RESET',
  lap: 'LAP',
  workoutName: 'Workout Name',
  workoutTime: 'Workout Time',
  restTime: 'Rest Time',
  totalRounds: 'No. of Rounds',
  save: 'SAVE',
  cancel: 'CANCEL',
  ok: 'OK',

  countUp: 'COUNT UP',
  countDown: 'COUNT DOWN',
}
